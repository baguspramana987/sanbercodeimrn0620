import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import YoutubeUI from './Tugas/Tugas12/App'
import Tugas14soal1 from './Tugas/Tugas14/component/Main'
import Tugas14soal2 from './Tugas/Tugas14/App'
import Tugas15 from './Tugas/Tugas15/index'
import TugasNav from './Tugas/TugasNavigation/index'
import Quiz from './Tugas/Quiz3/index'

export default function App() {
  return (
    <Quiz />
    //<YoutubeUI />
    // <ScrollView>
    // <View style={styles.container}>
    //   <Text>Hello World</Text>
    //   <StatusBar style="auto" />
    // </View>
    // </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
