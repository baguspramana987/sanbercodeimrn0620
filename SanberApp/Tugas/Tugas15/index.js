import React from "react";
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from "@react-navigation/drawer"

import { SignIn, CreateAccount, Search, Home, Details, Search2, Profile } from "./Screen"

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const HomeStack = createStackNavigator()
const SearchStack = createStackNavigator()

const HomeStackScreen = () => {
  return(
  <HomeStack.Navigator>
    <HomeStack.Screen name = 'Home' component = {Home} />
    <HomeStack.Screen name = 'Details' component = {Details}
    options={({route}) => ({title: route.params.name})} />
  </HomeStack.Navigator>
)
}

const ProfileStack = createStackNavigator()
const ProfileStackScreen=() => {
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
}

const SearchStackScreen = () =>{
  return(
  <SearchStack.Navigator>
    <SearchStack.Screen name = 'Search' component={Search} />
    <SearchStack.Screen name = 'Search2' component={Search2} />
  </SearchStack.Navigator>
)
}

const TabsScreen = () =>{
  return(
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Search" component={SearchStackScreen} />
  </Tabs.Navigator>
)
}

const Drawer= createDrawerNavigator();

export default() => {
  return(
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={TabsScreen} />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
  </NavigationContainer>
)
}
