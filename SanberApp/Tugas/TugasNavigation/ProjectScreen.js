import React from "react";
import { StyleSheet, Text, View } from "react-native";

export const Project = ({navigation}) => (
    <View style={styles.container}>
      <Text>Halaman Proyek</Text>
    </View>
  );

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
