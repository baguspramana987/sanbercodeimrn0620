import React, { Component } from 'react'
//import React from 'react'
import {
  TextInput,
  Platform,
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const About = ({navigation}) => (
      <View style={styles.container}>
        <View style={styles.regView}>
          <Text style={styles.regText}>Tentang Saya</Text>
        </View>
        <View style={styles.logo}>
          <Image source={require('./images/user.png')} style={{ width: 170, height: 170 }} />
        </View>
        <View style={styles.namaView}>
          <Text style={styles.namaText}>Bagus Pramana</Text>
        </View>
        <View style={styles.jbtView}>
          <Text style={styles.jbtText}>React Native Developer</Text>
        </View>

        <View style={styles.optionContainer}>
          <Text stroke="black" strokeWidth="1">     Portofolio</Text>
          <View style={styles.gambar1}>
            <Image style={styles.gambar1} source={require('./images/gitlab.png')} style={{ width: 50, height: 50 }} />
            <Text>@bagus</Text>
          </View>
          <View style={styles.gambar2}>
            <Image source={require('./images/github.png')} style={{ width: 50, height: 50 }} />
            <Text>@bagus</Text>
          </View>

        </View>

        <View style={styles.optionContainer1}>
          <View>
            <Text>     Hubungi Saya</Text>
          </View>
          <View style={styles.gambar3}>
            <Image source={require('./images/facebook.png')} style={{ width: 50, height: 50 }} />
            <Text style={styles.text1}>@bagus</Text>
            <Image source={require('./images/ig.png')} style={{ width: 50, height: 50 }} />
            <Text style={styles.text2}>@bagus</Text>
            <Image source={require('./images/twit.png')} style={{ width: 50, height: 50 }} />
            <Text style={styles.text3}>@bagus</Text>
          </View>
        </View>
      </View>
    )

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent: 'center'
  },
  text1:{
    position: 'absolute',
    left: '20.03%',
right: '3.22%',
top: '10.99%',
bottom: '6.51%',
},
text2:{
  position: 'absolute',
  left: '20.03%',
right: '3.22%',
top: '43.99%',
bottom: '6.51%',
},
text3:{
  position: 'absolute',
  left: '20.03%',
right: '3.22%',
top: '76.99%',
bottom: '6.51%',
},
  gambar1:{
    position: 'absolute',
    left: '20.03%',
right: '3.22%',
top: '20.99%',
bottom: '6.51%',
},
gambar2:{
  position: 'absolute',
left: '60.25%',
right: '20.25%',
top: '20.25%',
bottom: '20.25%'
},
gambar3:{
  position: 'absolute',
  left: '20.03%',
right: '3.22%',
top: '20.99%',
bottom: '6.51%',
},
  optionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 25,
    height: 100,
    marginBottom: 20,
  },
  optionContainer1: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 25,
    height: 200,
    marginBottom: 20,
  },
  borderr: {
    borderColor: 'black',
    borderWidth: 1
  },
  inputView: {
    //width: "80%",
    backgroundColor: "#EFEFEF",
    //borderRadius: 10,
    height: 100,
    //marginBottom: 20,
    //marginTop:5,
    justifyContent: "center",
    padding: 20
  },
  namaView:{
    fontWeight: "bold",
    fontSize:12,
    color:"#003366",
    marginBottom:5
  },
  namaText:{
    color: "#003366",
    fontSize: 24
  },
  jbtView:{
    fontWeight: "bold",
    fontSize:12,
    color:"#3EC6FF",
    marginBottom:20
  },
  jbtText:{
    color: "#3EC6FF",
    fontSize: 18
  },
  regview:{
    marginTop:20,
    marginBottom:50
  },
  regText:{
    marginBottom:20,
    color: "#003366",
    fontSize:40
  },
  inputText: {
    height: 50,
    color: "white"
  },
  logo:{
    marginBottom:30
  },
  daftarButton:{
    width: "80%",
    backgroundColor: "#003366",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10
  },
  daftarText:{
    color: "white"
  },
  loginButton:{
    width: "80%",
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText:{
    color: "white"
  }
})
