import React, { Component } from 'react'
//import React from 'react'
import {
  TextInput,
  Platform,
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AuthContext } from "./context";

export const Login = ({navigation}) => (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('./sanber.png')} style={{ width: 300, height: 90 }} />
        </View>
        <View style={styles.regView}>
          <Text style={styles.regText}>Login</Text>
        </View>
        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Username/Email" placeholderTextColor="#003f5c" onChangeText={text=>this.setState({username:text})}/>
        </View>

        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Password" placeholderTextColor="#003f5c" secureTextEntry={true} onChangeText={text=>this.setState({password:text})}/>
        </View>
        <TouchableOpacity style={styles.loginButton} onPress={() => navigation.push("Masuk")}>
          <Text style={styles.loginText}>Masuk ?</Text>
        </TouchableOpacity>
        <View style={styles.tView}>
          <Text style={styles.tText}>Atau</Text>
        </View>
        <TouchableOpacity style={styles.daftarButton}>
          <Text style={styles.daftarText}>Daftar</Text>
        </TouchableOpacity>


      </View>
    )

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent: 'center'
  },
  inputView: {
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    //marginTop:5,
    justifyContent: "center",
    padding: 20
  },
  tview:{
    fontWeight: "bold",
    fontSize:12,
    color:"#3EC6FF",
    marginBottom:20
  },
  tText:{
    color: "#3EC6FF"
  },
  regview:{
    marginTop:20,
    marginBottom:50
  },
  regText:{
    marginBottom:20,
    color: "#3EC6FF",
    fontSize:26
  },
  inputText: {
    height: 50,
    color: "black"
  },
  logo:{
    marginBottom:30
  },
  daftarButton:{
    width: "80%",
    backgroundColor: "#003366",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10
  },
  daftarText:{
    color: "white"
  },
  loginButton:{
    width: "80%",
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText:{
    color: "white"
  }
})
