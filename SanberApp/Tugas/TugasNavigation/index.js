import React from "react";
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from "@react-navigation/drawer"

import { Login } from "./LoginScreen"
import { Masuk } from "./SkillScreen"
import { About } from "./AboutScreen"
import { Project } from "./ProjectScreen"
import { Add } from "./AddScreen"

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const HomeStack = createStackNavigator()
const SearchStack = createStackNavigator()
const Drawer= createDrawerNavigator();

const TabsScreen = () =>{
  return(
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Masuk} />
      <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
)
}


export default() => {
  return(
  <NavigationContainer>
  <AuthStack.Navigator>
    <AuthStack.Screen
      name = "Login"
      component={Login}
      options={{ title: 'Sign In' }}/>
      <AuthStack.Screen
        name = "Masuk"
        component={Masuk}
        options={{ title: 'Beranda' }}/>
        <AuthStack.Screen
          name = "About"
          component={About}
          options={{ title: 'About' }}/>
          <AuthStack.Screen
            name = "Project"
            component={Project}
            options={{ title: 'Project' }}/>
            <AuthStack.Screen
              name = "Add"
              component={Add}
              options={{ title: 'Halaman Tambah' }}/>
  </AuthStack.Navigator>
  </NavigationContainer>
)
}
