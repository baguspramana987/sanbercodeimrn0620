/*

  A. String Terbalik (10 poin)
    Diketahui sebuah function terbalik() yang menerima satu buah parameter berupa tipe data string. Function terbalik() 
    akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. 
    contoh: terbalik("Javascript") akan me-return string "tpircsavaJ", terbalik("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  B. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama maksimum() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 

  C. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 
  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
    

function terbalik() {
  // Silakan tulis code kamu di sini
}

function maksimum(num1, num2) {
  // code di sini
}

function palindrome() {
  // Silakan tulis code kamu di sini
}

// TEST CASES String Terbalik
console.log(terbalik("abcde")) // edcba
console.log(terbalik("rusak")) // kasur
console.log(terbalik("racecar")) // racecar
console.log(terbalik("haji")) // ijah

// TEST CASES Bandingkan Angka
console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1 
console.log(maksimum(112, 121));// 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")) // 18

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


*/
console.log('\nA. String terbalik\n')

function terbalik(kata){
	var akhir=''
	for(i = kata.length-1;i>=0;i--){
		akhir = akhir+kata.charAt(i)
	}
	return akhir
}

console.log(terbalik("Javascript"))

console.log('\nB. Bandingkan angka\n')

function maksimum(a,b){
	if(a<0 || b<0){
		return -1
	}else if(a==b){
		return -1
	}else{
		if(a>b){
			return a
		}else{
			return b
		}
	}
}

console.log(maksimum(1,3))
console.log(maksimum(5,3))
console.log(maksimum(-1,3))
console.log(maksimum(3,3))

console.log('\nC. Palindrome\n')

function palindrome(kalimat) {
	var car = /[^A-Za-z0-9]/g;
	kalimat = kalimat.toLowerCase().replace(car, '');
	var len = kalimat.length;
	for (var i = 0; i < len/2; i++) {
		if (kalimat[i] !== kalimat[len - 1 - i]) {
       			return false;
   		}
 	}
 	return true;
}

console.log(palindrome("kasur rusak"))
console.log(palindrome("jakarta"))
true
console.log(palindrome("haji ijah"))
true
console.log(palindrome("nabasan"))
false
console.log(palindrome("nababan"))
true
console.log(palindrome("jakarta"))



