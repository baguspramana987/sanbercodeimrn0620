import React, { Component } from 'react'
//import React from 'react'
import {
  TextInput,
  Platform,
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './component/videoItem'
import data from './data.json'

export default class App extends React.Component {
  render() {
    //alert(data.kind);
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('./images/sanber.png')} style={{ width: 300, height: 90 }} />
        </View>
        <View style={styles.regView}>
          <Text style={styles.regText}>Register</Text>
        </View>
        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Username" placeholderTextColor="#003f5c" onChangeText={text=>this.setState({username:text})}/>
        </View>
        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Email" placeholderTextColor="#003f5c" onChangeText={text=>this.setState({email:text})}/>
        </View>
        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Password" placeholderTextColor="#003f5c" secureTextEntry={true} onChangeText={text=>this.setState({password:text})}/>
        </View>
        <View style={styles.inputView}>
          <TextInput style={styles.inputText} placeholder="Ulangi Password" placeholderTextColor="#003f5c" secureTextEntry={true} onChangeText={text=>this.setState({upassword:text})}/>
        </View>
        <TouchableOpacity style={styles.daftarButton}>
          <Text style={styles.daftarText}>Daftar</Text>
        </TouchableOpacity>
        <View style={styles.tView}>
          <Text style={styles.tText}>Atau</Text>
        </View>
        <TouchableOpacity style={styles.loginButton}>
          <Text style={styles.loginText}>Masuk ?</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent: 'center'
  },
  inputView: {
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    //marginTop:5,
    justifyContent: "center",
    padding: 20
  },
  tview:{
    fontWeight: "bold",
    fontSize:12,
    color:"#3EC6FF",
    marginBottom:20
  },
  tText:{
    color: "#3EC6FF"
  },
  regview:{
    marginTop:20,
    marginBottom:50
  },
  regText:{
    marginBottom:20,
    color: "#3EC6FF",
    fontSize:26
  },
  inputText: {
    height: 50,
    color: "black"
  },
  logo:{
    marginBottom:30
  },
  daftarButton:{
    width: "80%",
    backgroundColor: "#003366",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  daftarText:{
    color: "white"
  },
  loginButton:{
    width: "80%",
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10
  },
  loginText:{
    color: "white"
  }
})

// export default class App extends React.Component {
//   render() {
//     //alert(data.kind);
//     return (
//       <View style={styles.container}>
//         <View style={styles.navBar}>
//           <Image source={require('./images/logo.png')} style={{ width: 98, height: 22 }} />
//           <View style={styles.rightNav}>
//             <TouchableOpacity><Icon style={styles.navItem} name="search" size={25} /></TouchableOpacity>
//             <TouchableOpacity><Icon style={styles.navItem} name="account-circle" size={25} /></TouchableOpacity>
//           </View>
//         </View>
//         <View style={styles.body}>
//           <FlatList data={data.items}
//             renderItem={(video) => <VideoItem video={video.item} />}
//             keyExtractor={(item) => item.id}
//             ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
//           />
//         </View>
//         <View style={styles.tabBar}>
//           <TouchableOpacity style={styles.tabItem}>
//             <Icon name="home" size={25}></Icon>
//             <Text style={styles.tabTitle}>Home</Text>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.tabItem}>
//             <Icon name="whatshot" size={25} />
//             <Text style={styles.tabTitle}>Trending</Text>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.tabItem}>
//             <Icon name="subscriptions" size={25} />
//             <Text style={styles.tabTitle}>Subscriptions</Text>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.tabItem}>
//             <Icon name="folder" size={25} />
//             <Text style={styles.tabTitle}>Library</Text>
//           </TouchableOpacity>
//         </View>
//       </View>
//     )
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   },
//   navBar: {
//     height: 55,
//     backgroundColor: 'white',
//     elevation: 3,
//     paddingHorizontal: 15,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between'
//   },
//   rightNav: {
//     flexDirection: 'row'
//   },
//   navItem: {
//     marginLeft: 25
//   },
//   body: {
//     flex: 1
//   },
//   tabBar: {
//     backgroundColor: 'white',
//     height: 60,
//     borderColor: '#F5F5F5',
//     borderTopWidth: 0.5,
//     flexDirection: 'row',
//     justifyContent: 'space-around'
//   },
//   tabItem: {
//     alignItems: 'center',
//     justifyContent: 'center'
//   },
//   tabTitle: {
//     fontSize: 11,
//     color: '#3c3c3c',
//     paddingTop: 4
//   }
// })
