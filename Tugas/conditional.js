console.log("1. Conditional if else\n")
var nama = "Jean"
var peran = ""

if (nama == ""){
	console.log("Nama Harus diisi!\n")
}else if(peran == "" && nama == "John"){
	console.log("Halo John, Pilih Peranmu untuk memulai game!\n")
}else if(peran == ""){
	console.log("Pilih peranmu untuk memulai game\n")
}else{
	if (nama == "Jane" && peran == "Penyihir"){
	console.log("Selamat datang di Dunia Werewolf, Jane");
	console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!\n");
	}else if(nama == "Jenita" && peran =="Guard"){
	console.log("Selamat datang di Dunia Werewolf, Jenita");
	console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.\n");
	}else if(nama == "Junaedi" && peran =="Werewolf"){
	console.log("Selamat datang di Dunia Werewolf, Junaedi");
	console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!\n");
	}else{
	console.log("Halo "+peran+" "+nama+"\n");
	}
}

console.log("2. Switch Case\n")
var hari = 21;
var bulan = 5;
var tahun = 1945;

if (hari>=1 && hari<=31){
	if (tahun>=1900 && tahun<=2200){
		switch(bulan){
			case 1: {console.log(hari+" Januari "+tahun);break;}
			case 2: {console.log(hari+" Februari "+tahun);break;}
			case 3: {console.log(hari+" Maret "+tahun);break;}
			case 4: {console.log(hari+" April "+tahun);break;}
			case 5: {console.log(hari+" Mei "+tahun);break;}
			case 6: {console.log(hari+" Juni "+tahun);break;}
			case 7: {console.log(hari+" Juli "+tahun);break;}
			case 8: {console.log(hari+" Agustus "+tahun);break;}
			case 9: {console.log(hari+" September "+tahun);break;}
			case 10: {console.log(hari+" Oktober "+tahun);break;}
			case 11: {console.log(hari+" November "+tahun);break;}
			case 12: {console.log(hari+" Desember "+tahun);break;}
			default: {console.log("Bulan tidak valid");break;}
		}
	}else{
		console.log("Tahun harus diantara 1900-2200");
	}
}else{
	console.log("Tanggal tidak valid");
}