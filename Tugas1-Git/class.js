console.log("1. Animal Class\n")
console.log("Release 0\n")
class Animal{
	constructor(name){
		this.animalname = name
	}
	get nama(){
		return this.animalname
	}
	set nama(y){
		this.animalname = y
	}
	get legs(){
		return 4
	}
	get cold_blooded(){
		return false
	}
}

var sheep = new Animal("Shaun")

console.log(sheep.nama)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

console.log("\nRelease 1\n")

class Hewan{
	constructor(nama){
		this.namahewan = nama
	}
	present(){
		return this.namahewan
	}
}

class Kaki2 extends Hewan{
	constructor(nama){
		super(nama)
	}
}

class Ape extends Kaki2{
	constructor(nama){
		super(nama)
	}
	yell(){
		console.log('Auooo')
	}
}

class Kaki4 extends Hewan{
	constructor(nama){
		super(nama)
	}
}

class Frog extends Kaki4{
	constructor(nama){
		super(nama)
	}
	jump(){
		console.log('hop hop')
	}
}

var sungokong = new Ape("kera sakti")
sungokong.yell()
 
var kodok = new Frog("buduk")
kodok.jump()

console.log("\n2. Function to Class\n")

class Clock{
	constructor({template}){
		this.template=template
		this.timer
	}
	start() {
    		this.render();
    		this.timer = setInterval(this.render.bind(this), 1000);
  	}
	
	render(){
    		var date = new Date();

    		var hours = date.getHours();
    		if (hours < 10) hours = '0' + hours;

    		var mins = date.getMinutes();
    		if (mins < 10) mins = '0' + mins;

    		var secs = date.getSeconds();
    		if (secs < 10) secs = '0' + secs;
		
		var output = this.template
      		.replace('h', hours)
      		.replace('m', mins)
      		.replace('s', secs);

    		console.log(output);
  	}

  	stop() {
    		return clearInterval(timer);
  	}

  	
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  