console.log("1. Array to Object\n")

function arrayToObject(arr) {
	if(arr==null){
		return '""'
	}else{
	var now = new Date()
	var thisYear = now.getFullYear()


	for(var i=0;i<arr.length;i++){
		var obj = {};var j=0
		obj.firstname = arr[i][j++]
		obj.lastname = arr[i][j++]
		obj.gender = arr[i][j++]
		
		if(thisYear>arr[i][j]){
			var tahun = thisYear-arr[i][j]
		}else{
			var tahun = "Invalid Birth Year"
		}
		obj.age = tahun
		var no = i+1
		console.log(no+". "+obj.firstname+" "+obj.lastname+" : ")
		console.log(obj)
	}
	}
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
arrayToObject([])

console.log("\n2. Shopping Time\n")

function shoppingTime(memberId, money){
var product =[["Stacattu",1500000],["Zoro",500000],["H&N",250000],["Uniklooh",175000],["Casing Handphone",50000]]
	if (memberId=='' || money==undefined){
		return "Mohon Maaf, Toko X hanya berlaku untuk member saja"
	}else if(money<50000){
		return "Mohon Maaf uang anda tidak cukup"
	}else{	
		var uang = money
		var i = 0;var ar = [];var j = 0
		while(money>=50000 && i<product.length){
			if(money>=product[i][1]){
			money = money-product[i][1]
			ar[j] = product[i][0]
			j++
			}
			i++
		}
		var struk = {
			memberId : memberId,
			money : uang,
			listPurchased : ar,
			changeMoney : money
		}
		return struk
	}
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log("\n3. Naik angkot\n")

function naikAngkot(arrPenumpang) {
  	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  	if(arrPenumpang==null){
		return "[]"
	}else{
		var bayar = 2000
		var a = []
		for(i=0;i<arrPenumpang.length;i++){
			b=rute.indexOf(arrPenumpang[i][1])
			c=rute.indexOf(arrPenumpang[i][2])
			d=(c-b)*bayar
			j=0;a[i]={
			penumpang : arrPenumpang[i][j++],
			naikDari : arrPenumpang[i][j++],
			tujuan : arrPenumpang[i][j++],
			bayar : d
			}
		}
		return a
	}
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))