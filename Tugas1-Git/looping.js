console.log('1. Looping while\n')
var loop=2;
console.log('LOOPING PERTAMA')
while(loop<=20){
	console.log(loop+' - I love coding')
	loop += 2
}
console.log('\nLOOPING KEDUA')
while(loop>2){
	loop -= 2
	console.log(loop+' - I will become a mobile developer')
}

console.log('\n2. Looping menggunakan for\n')
for(var i=1;i<=20;i++){
	if(i%3==0 && i%2==1){
		console.log(i + ' - I Love Coding');
	}else if(i%2==0){
		console.log(i + ' - Berkualitas');
	}else{
		console.log(i + ' - Santai');
	}
}

console.log('\n3. Membuat Persegi Panjang\n')

i = 0
while(i<4){
	var pagar='#'
	for(var p=1;p<=8;p++){
		pagar=pagar+'#'
	}
	console.log(pagar)
	i++
}

console.log('\n4. Membuat tangga\n')

var bintang = '#'
var i = 0
while(i<7){
	console.log(bintang);
	bintang=bintang+'#';
	i++
}

console.log('\n5. Membuat papan catur\n')
var pagar = '#'
var spasi = ' '
i = 0
while(i<8){
	var temppagar = pagar
	var tempspasi = spasi
	var catur = ''
	for(var p=0;p<4;p++){
		catur=catur+tempspasi
		catur=catur+temppagar
	}
	pagar=tempspasi
	spasi=temppagar	
	console.log(catur)
	i++
}